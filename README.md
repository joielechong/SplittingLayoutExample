A sample project with multi layout

When we have large Android project, it is becomes difficult to manage many layouts for
the UI because we can only place the layout inside one folder, which is layout resource folder.

So, we need to separate it to multi-folder layouts.

Here the step:

1. Change app build.gradle by adding sourceSets block to something like this, where we have 2 different purpose layout folder:

        sourceSets {
            main {
                res.srcDirs = [
                    'src/main/res',
                    'src/main/res/layouts',
                    'src/main/res/layouts/main',
                    'src/main/res/layouts/other',]
            }
        }

    First, the layout for main. Second the layout for other.

2. Create layouts folder and the subfolder for the main and other with this hierarchy:

        src/main/res/layouts
                    -> main
                           -> layout

                    -> other
                           -> layout

3. Then place the xml files to respective folder inside the layout. For example, if layout
xml is for main, place it inside this folder:

        src/main/res/layouts/main/layout

4. Sync and finish.